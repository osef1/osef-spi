#!/bin/bash

lizard --version

lizard --languages cpp --modified --CCN 8 --length 80 --arguments 4 --sort cyclomatic_complexity --Threshold nloc=70 "$@"
