#include "SpiDevice.h"
#include "Swap.h"
#include "Debug.h"

#include <unistd.h>  // close
#include <fcntl.h>  // O_RDWR
#include <sys/ioctl.h>  // ioctl
#include <limits>

OSEF::SpiDevice::SpiDevice(const SpiChip& c, const uint8_t& m, const uint32_t& s, const uint8_t& bps)
    :file(-1),
    peripheral(c.peripheral),
    chip(c.chip),
    setupOk(false),
    mode(m),
    speed(s),
    bitsPerWord(bps),
    iotf() {}

OSEF::SpiDevice::~SpiDevice()
{
    if (file >= 0)
    {
        if (close(file) != 0)
        {
            DERR("error closing spi device " << peripheral << "." << chip);
        }
    }
}

bool OSEF::SpiDevice::setup()
{
    if (not setupOk)
    {
        std::string filename = "/dev/spidev" + std::to_string(peripheral) + "." + std::to_string(chip);
        file = open(filename.c_str(), O_RDWR | O_CLOEXEC);
        if (file >= 0)
        {
            if (setMode(mode))
            {
                if (setSpeed(speed))
                {
                    if (setBitsPerWord(bitsPerWord))
                    {
                        DOUT("spi device " << peripheral << "." << chip << " ready");
                        setupOk = true;
                    }
                }
            }

            if (not setupOk)
            {
                if (close(file) != 0)
                {
                    DERR("error closing spi device " << peripheral << "." << chip);
                }
            }
        }
        else
        {
            SPER("error opening spi device file " << filename);
        }

        memset(&iotf, 0, sizeof(iotf));
    }

    return setupOk;
}

bool OSEF::SpiDevice::setMode(const uint8_t& mode) const
{
    bool ret = false;

    if (ioctl(file, SpiWriteMode, &mode) == 0)
    {
        uint8_t rm = 0;
        if (ioctl(file, SpiReadMode, &rm) == 0)
        {
            if ((rm&0x3) == mode)
            {
                DOUT("spi mode = " << +rm);
                ret = true;
            }
            else
            {
                SPER("spi mode update error " << +(rm&0x3) << "!=" << +mode);
            }
        }
        else
        {
            SPER("spi mode read error");
        }
    }
    else
    {
        SPER("spi mode write error");
    }

    return ret;
}

bool OSEF::SpiDevice::setSpeed(const uint32_t& speed) const
{
    bool ret = false;

    if (ioctl(file, SpiWriteMaxSpeedHz, &speed) == 0)
    {
        uint32_t rs = 0;
        if (ioctl(file, SpiReadMaxSpeedHz, &rs) == 0)
        {
            if (rs == speed)
            {
                DOUT("spi speed = " << speed);
                ret = true;
            }
            else
            {
                SPER("spi speed update error " << +rs << "!=" << speed);
            }
        }
        else
        {
            SPER("spi speed read error");
        }
    }
    else
    {
        SPER("spi speed write error");
    }

    return ret;
}

bool OSEF::SpiDevice::setBitsPerWord(const uint8_t& bpw) const
{
    bool ret = false;

    if (ioctl(file, SpiWriteBitsPerWord, &bpw) == 0)
    {
        uint8_t rbpw = 0;
        if (ioctl(file, SpiReadBitsPerWord, &rbpw) == 0)
        {
            if (rbpw == bpw)
            {
                DOUT("spi bits per word = " << +bpw);
                ret = true;
            }
            else
            {
                SPER("spi bits per word update error " << +rbpw << "!=" << bpw);
            }
        }
        else
        {
            SPER("spi bits per word read error");
        }
    }
    else
    {
        SPER("spi bits per word write error");
    }

    return ret;
}

bool OSEF::SpiDevice::exchangeMessage(const uint64_t& rb, const uint64_t& tb, const uint32_t& lg)
{
    bool ret = false;

    if (setup())
    {
        iotf.rx_buf = rb;
        iotf.tx_buf = tb;
        iotf.len = lg;

        if (ioctl(file, SpiIoctlMsg1, &iotf) >= 0)
        {
            ret = true;
        }
        else
        {
            SPER("error on spi errno = " << strerror(errno));
        }
    }

    return ret;
}

bool OSEF::SpiDevice::readByte(uint8_t& value)
{
    const bool ret = exchangeMessage((uint64_t)&value, 0, 1);
    return ret;
}

bool OSEF::SpiDevice::writeByte(const uint8_t& value)
{
    const bool ret = exchangeMessage(0, (uint64_t)&value, 1);
    return ret;
}

bool OSEF::SpiDevice::exchangeByte(const uint8_t& tx, uint8_t& rx)
{
    const bool ret = exchangeMessage((uint64_t)&rx, (uint64_t)&tx, 1);
    return ret;
}

bool OSEF::SpiDevice::readWord(uint16_t& value)
{
    const bool ret = exchangeMessage((uint64_t)&value, 0, 2);
    return ret;
}

bool OSEF::SpiDevice::writeWord(const uint16_t& value)
{
    const bool ret = exchangeMessage(0, (uint64_t)&value, 2);
    return ret;
}

bool OSEF::SpiDevice::writeString(const std::string& s)
{
    bool ret = false;

    if (s.size() <= std::numeric_limits<uint32_t>::max() )
    {
        ret = exchangeMessage(0, (uint64_t)s.c_str(), s.size());
    }
    else
    {
        DERR("payload too long (" << s.size() << " > " << std::numeric_limits<uint32_t>::max() << ")");
    }

    return ret;
}

bool OSEF::SpiDevice::readWordSwap(uint16_t& value)
{
    const bool ret = readWord(value);
    value = swap16(value);
    return ret;
}

bool OSEF::SpiDevice::writeWordSwap(const uint16_t& value)
{
    bool ret = writeWord(swap16(value));
    return ret;
}

//    bool OSEF::SpiDevice::getSpiPeripheralChip(int argc, char** argv, uint32_t& periph, uint32_t& cs)
//    {
//        bool ret = true;
//
//        if (argc > 1)
//        {
//            uint64_t tmp = 0;
//            int32_t opt = getopt(argc, argv, "p:c:");
//            while (opt != -1)
//            {
//                switch (opt)
//                {
//                    case 'p':
//                        tmp = std::strtoul(optarg, nullptr, 10);
//                        if (tmp <= 0xffUL)
//                        {
//                            periph = tmp;
//                        }
//                        else
//                        {
//                            ret = false;
//                        }
//                        break;
//                    case 'c':
//                        tmp = std::strtoul(optarg, nullptr, 10);
//                        if (tmp <= 0xffUL)
//                        {
//                            cs = tmp;
//                        }
//                        else
//                        {
//                            ret = false;
//                        }
//                        break;
//                    default:
//                        ret = false;
//                        break;
//                }
//
//                opt = getopt(argc, argv, "p:c:");
//            }
//        }
//
//        if (not ret)
//        {
//            std::cout << "Usage:   " << argv[static_cast<size_t>(0)] << " [-option] [argument]" << std::endl;
//            std::cout << "option:  " << std::endl;
//            std::cout << "         " << "-p  SPI peripheral [0-255]" << std::endl;
//            std::cout << "         " << "-c  SPI chip select [0-255]" << std::endl;
//        }
//
//        return ret;
//    }
