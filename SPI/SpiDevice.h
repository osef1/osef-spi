#ifndef OSEFSPIDEVICE_H
#define OSEFSPIDEVICE_H

#include <linux/spi/spidev.h>  // spi_ioc_transfer

#include <string>

#ifndef SPER
#   ifdef DEBUG
#       define SPER(s) std::cerr << "\033[31;40m" << __LINE__ << " " << __PRETTY_FUNCTION__ << " " << s << " device " << +peripheral << "." << +chip << "\033[0;0m" << std::endl
#   else
#       define SPER(s)
#   endif
#endif

namespace OSEF
{
    const uint32_t SpiWriteMode = 0x40016b01;  // SPI_IOC_WR_MODE
    const uint32_t SpiReadMode = 0x80016b01;  // SPI_IOC_RD_MODE
    const uint32_t SpiWriteMaxSpeedHz = 0x40046b04;  // SPI_IOC_WR_MAX_SPEED_HZ
    const uint32_t SpiReadMaxSpeedHz = 0x80046b04;  // SPI_IOC_RD_MAX_SPEED_HZ
    const uint32_t SpiWriteBitsPerWord = 0x40016b03;  // SPI_IOC_WR_BITS_PER_WORD
    const uint32_t SpiReadBitsPerWord = 0x80016b03;  // SPI_IOC_RD_BITS_PER_WORD
    const uint32_t SpiIoctlMsg1 = 0x40206b00;  // SPI_IOC_MESSAGE(1)

    struct SpiChip
    {
        uint32_t peripheral;
        uint32_t chip;
    };

    class SpiDevice
    {
    public:
        SpiDevice(const SpiChip& c, const uint8_t& m, const uint32_t& s, const uint8_t& bps = 8);
        virtual ~SpiDevice();

        bool isPeripheralOK() const {return (file >= 0);}

        bool readByte(uint8_t& value);
        bool writeByte(const uint8_t& value);
        bool exchangeByte(const uint8_t& tx, uint8_t& rx);

        bool readWord(uint16_t& value);
        bool writeWord(const uint16_t& value);

        bool readWordSwap(uint16_t& value);
        bool writeWordSwap(const uint16_t& value);

        bool writeString(const std::string& s);

//        static bool getSpiPeripheralChip(int argc, char** argv, uint32_t& periph, uint32_t& cs);

        SpiDevice(const SpiDevice&) = delete;  // copy constructor
        SpiDevice& operator=(const SpiDevice&) = delete;  // copy assignment
        SpiDevice(SpiDevice&&) = delete;  // move constructor
        SpiDevice& operator=(SpiDevice&&) = delete;  // move assignment

    private:
        bool setup();

        bool setMode(const uint8_t& mode) const;
        bool setSpeed(const uint32_t& speed) const;
        bool setBitsPerWord(const uint8_t& bpw) const;

        bool exchangeMessage(const uint64_t& rb, const uint64_t& tb, const uint32_t& lg);

        int file;

        uint32_t peripheral;
        uint32_t chip;

        bool setupOk;

        uint8_t mode;
        uint32_t speed;
        uint8_t bitsPerWord;

        spi_ioc_transfer iotf;
    };
}  // namespace OSEF

#endif /* OSEFSPIDEVICE_H */
