#!/bin/bash

mkdir -p build/release

cd build/release

cmake -DCMAKE_BUILD_TYPE=release -DCMAKE_LINK_WHAT_YOU_USE=TRUE ../..

make
